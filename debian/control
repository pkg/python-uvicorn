Source: python-uvicorn
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Michael Fladischer <fladi@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               docbook-to-man,
#               mkdocs,
               python3-all,
               python3-anyio <!nocheck>,
               python3-asgiref <!nocheck>,
               python3-click,
               python3-dotenv <!nocheck>,
               python3-h11,
               python3-httptools,
               python3-httpx (>= 0.18.2) <!nocheck>,
               python3-pytest <!nocheck>,
               python3-pytest-asyncio <!nocheck>,
               python3-pytest-mock <!nocheck>,
               python3-requests,
               python3-setuptools,
               python3-trustme <!nocheck>,
               python3-uvloop,
               python3-watchgod,
               python3-websockets,
               python3-wsproto,
               python3-yaml
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-uvicorn
Vcs-Git: https://salsa.debian.org/python-team/packages/python-uvicorn.git
Homepage: https://github.com/encode/uvicorn/
Rules-Requires-Root: no

Package: python-uvicorn-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${mkdocs:Depends}
Description: ASGI server implementation, using uvloop and httptools (Documentation)
 Uvicorn is a fast ASGI server, built on uvloop and httptools. It currently
 supports HTTP/1.1 and WebSockets.
 .
 Uvicorn is designed with particular attention to connection and resource
 management, in order to provide a robust server implementation. It aims to
 ensure graceful behavior to either server or client errors, and resilience to
 poor client behavior or denial of service attacks.
 .
 This package contains the documentation.

Package: python3-uvicorn
Architecture: all
Depends: python3-wsproto,
         ${misc:Depends},
         ${python3:Depends}
Suggests: python-uvicorn-doc
Enhances: python3-gunicorn
Description: ASGI server implementation, using uvloop and httptools (Python3 version)
 Uvicorn is a fast ASGI server, built on uvloop and httptools. It currently
 supports HTTP/1.1 and WebSockets.
 .
 Uvicorn is designed with particular attention to connection and resource
 management, in order to provide a robust server implementation. It aims to
 ensure graceful behavior to either server or client errors, and resilience to
 poor client behavior or denial of service attacks.
 .
 This package contains the Python 3 version of the library.

Package: uvicorn
Architecture: all
Section: httpd
Depends: python3-uvicorn (= ${binary:Version}),
         ${misc:Depends},
         ${python3:Depends}
Suggests: python-uvicorn-doc
Description: ASGI server implementation, using uvloop and httptools
 Uvicorn is a fast ASGI server, built on uvloop and httptools. It currently
 supports HTTP/1.1 and WebSockets.
 .
 Uvicorn is designed with particular attention to connection and resource
 management, in order to provide a robust server implementation. It aims to
 ensure graceful behavior to either server or client errors, and resilience to
 poor client behavior or denial of service attacks.
 .
 This package contains the CLI script.
